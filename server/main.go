package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"sync"

	pb "gitlab.com/darksome/protorepo/chat"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type clientsPool struct {
	channels map[string]chan *pb.Message
	*sync.RWMutex
}

func (c *clientsPool) safe(f func()) {
	c.Lock()
	f()
	c.Unlock()
}

func (c *clientsPool) Broadcast(message *pb.Message) {
	c.safe(func() {
		for _, ch := range c.channels {
			ch <- message
		}
	})
}

func (c *clientsPool) Register(name string) (ch chan *pb.Message, err error) {
	exist := false
	c.safe(func() {
		_, exist = c.channels[name]
		if exist {
			return
		}

		c.channels[name] = make(chan *pb.Message)
		ch = c.channels[name]
	})

	if exist {
		err = status.Errorf(codes.AlreadyExists, "such username is already taken")
	}

	return
}

func (c *clientsPool) Delete(name string) {
	c.safe(func() {
		delete(c.channels, name)
	})
}

func ClientsPool() *clientsPool {
	return &clientsPool{
		channels: make(map[string]chan *pb.Message),
		RWMutex:  new(sync.RWMutex),
	}
}

var clients = ClientsPool()

type server struct{}

func (s *server) MessageExchange(srv pb.Chat_MessageExchangeServer) error {
	ctx := srv.Context()
	session := make(chan error)

	var name string
	userMessages := 0

	go func() {
		for {
			select {
			case <-ctx.Done():
				session <- ctx.Err()
			default:
			}

			message, err := srv.Recv()
			if err == io.EOF {
				log.Println("exit")
				session <- nil
			}
			if err != nil {
				if status.Code(err) == codes.Canceled {
					log.Printf("%s disconnected", name)
					clients.Delete(name)

					session <- err
					return
				}
				log.Printf("receive error %v", err)
				continue
			}

			if len(message.Text) > 255 {
				st := status.New(codes.InvalidArgument, "bad request")
				details := &errdetails.BadRequest{}
				v := &errdetails.BadRequest_FieldViolation{
					Field:       "text",
					Description: "The message should be no longer than 255 characters",
				}
				details.FieldViolations = append(details.FieldViolations, v)
				st, err = st.WithDetails(details)
				if err != nil {
					panic(fmt.Sprintf("%v", err))
				}

				session <- st.Err()
				return
			}

			if userMessages == 0 {
				name = message.Name
				client, err := clients.Register(name)
				if err != nil {
					session <- err
					return
				}

				go func() {
					for message := range client {
						if err := srv.Send(message); err != nil {
							if status.Code(err) == codes.Canceled {
								session <- err
								return
							}
							log.Printf("send error %v", err)
							continue
						}
					}
				}()
			}

			log.Printf("%s: %s", message.Name, message.Text)
			userMessages++

			clients.Broadcast(message)
		}
	}()

	return <-session
}

func main() {
	lis, err := net.Listen("tcp", ":5555")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterChatServer(s, &server{})

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
