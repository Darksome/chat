package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"

	pb "gitlab.com/darksome/protorepo/chat"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter your nickname: ")
	nickname, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}
	nickname = strings.TrimSuffix(nickname, "\n")

	conn, err := grpc.Dial(":5555", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("can not connect with server %v", err)
	}

	client := pb.NewChatClient(conn)
	stream, err := client.MessageExchange(context.Background())
	if err != nil {
		log.Fatalf("openn stream error %v", err)
	}

	ctx := stream.Context()
	done := make(chan bool)

	go func() {
		defer stream.CloseSend()
		for {
			text, err := reader.ReadString('\n')
			if err != nil {
				panic(err)
			}
			text = strings.TrimSuffix(text, "\n")

			m := pb.Message{Name: nickname, Text: text}
			if err := stream.Send(&m); err != nil {
				log.Fatalf("can not send %v", err)
			}
			time.Sleep(time.Second)
		}
	}()

	go func() {
		for {
			m, err := stream.Recv()
			if err == io.EOF {
				close(done)
				return
			}
			if err != nil {
				st := status.Convert(err)
				code := st.Code()

				switch code {
				case codes.AlreadyExists:
					log.Printf("Username was already taken.")
				case codes.InvalidArgument:
					for _, detail := range st.Details() {
						switch t := detail.(type) {
						case *errdetails.BadRequest:
							for _, violation := range t.GetFieldViolations() {
								log.Printf(
									"The %q field was wrong: %s",
									violation.GetField(),
									violation.GetDescription(),
								)
							}
						}
					}
				default:
					log.Printf("Unexpected error: %v", err)
				}

				return
			}

			log.Printf("%s: %s", m.Name, m.Text)
		}
	}()

	go func() {
		<-ctx.Done()
		if err := ctx.Err(); err != nil {
			log.Println(err)
		}
		close(done)
	}()

	<-done
}
